package com.smart.project.service;

import com.smart.project.model.User;

public interface UserService {
    void save(User user);

    User findByUsername(String username);
}