package com.smart.project.service;

import com.smart.project.model.Duration;

public interface DurationService {
    Duration getDurationById(long projectID);

}
