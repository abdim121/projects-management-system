package com.smart.project.service;


import com.smart.project.model.Duration;
import com.smart.project.model.Project;

import java.util.List;

public interface ProjectService {
    public Project insertProject(Project project);

    public List<Project> getAllProjects();

    public void deleteById(Long id);

    public Project findById(long projectId);

    public List<Duration> getDurationsByProjectId(Long projectId);

    public void deleteDurationById(Long id);

    public Project saveUpdate(Project project);

    public void insertDuration(Duration duration) ;

    public Duration findDurationById(Long durationId);

    public void saveUpdateDuration(Duration duration);

    List<Project> getProjectByUser(long userId);

}
