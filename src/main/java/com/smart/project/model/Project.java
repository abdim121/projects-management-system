package com.smart.project.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.smart.project.enums.CompleteStatus;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

import static javax.persistence.CascadeType.ALL;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Table(name = "tbl_project")
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "project_name")
    private String projectName;

    @Column(name = "description", length = 10000)
    private String projectDescription;

    @CreatedDate
    @CreationTimestamp
    @Column(name = "created_at")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Date date;

    @Column(name = "complete_status")
    private CompleteStatus completeStatus;

    @OneToMany(mappedBy = "project", fetch = FetchType.EAGER, cascade = ALL)
    @JsonManagedReference
    private List<Duration> durationList;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "user_id", nullable = false)
	private User user;
}
