package com.smart.project.service;

import com.smart.project.model.Duration;
import com.smart.project.model.Project;
import com.smart.project.repository.DurationRepository;
import com.smart.project.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProjectServiceImpl implements ProjectService{

    @Autowired
    ProjectRepository projectRepository;

    @Autowired
    DurationRepository durationRepository;

    @Override
    public Project insertProject(Project project) {
        return projectRepository.save(project);
    }

    @Override
    public List<Project> getAllProjects() {
        return projectRepository.findAll();
    }

    @Override
    public void deleteById(Long id) {
        projectRepository.deleteById(id);
    }

    @Override
    public Project findById(long projectId) {
        return projectRepository.getOne(projectId);
    }

    @Override
    public List<Duration> getDurationsByProjectId(Long projectId) {
        Project findProject = findById(projectId);
        return findProject.getDurationList();
    }

    @Override
    public void deleteDurationById(Long id) {
        durationRepository.deleteById(id);
    }

    @Override
    public Project saveUpdate(Project project) {
        return projectRepository.save(project);
    }

    @Override
    public void insertDuration(Duration duration) {
        durationRepository.save(duration);
    }

    @Override
    public Duration findDurationById(Long durationId) {
        return durationRepository.getOne(durationId);
    }

    @Override
    public void saveUpdateDuration(Duration duration) {
        durationRepository.save(duration);
    }

    @Override
    public List<Project> getProjectByUser(long userId) {
        return projectRepository.getProjectByUser(userId);
    }


}
