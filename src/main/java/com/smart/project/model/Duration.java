package com.smart.project.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.smart.project.enums.CompleteStatus;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Date;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Table(name = "tbl_duration")
public class Duration {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "duration_description")
    private String durationDescription;

    @Column(name = "complete_status")
    private CompleteStatus completeStatus;

    @Transient
    private long projectId;

    @UpdateTimestamp
    private Date lastUpdate;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "fk_duration")
    private Project project;

}
