package com.smart.project.repository;

import com.smart.project.model.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProjectRepository extends JpaRepository<Project, Long> {

    @Query("FROM Project WHERE user.id = ?1")
    List<Project> getProjectByUser(long userId);

    @Query("FROM Project p WHERE p.id= ?1")
    Project getProjectById(long id);
}
