package com.smart.project.service;

import com.smart.project.model.Duration;
import com.smart.project.repository.DurationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DurationServiceImpl implements DurationService{

    @Autowired
    DurationRepository durationRepository;

    @Override
    public Duration getDurationById(long projectID) {
        return durationRepository.getById(projectID);
    }
}
